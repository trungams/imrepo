#include <chrono>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "src/index.h"

std::vector<std::string> split(const std::string &str) {
  std::vector<std::string> tokens;
  std::istringstream iss(str);
  std::string word;
  while (iss >> word)
    tokens.push_back(std::move(word));
  return tokens;
}

void check_arg_count(const std::string &cmd, size_t exp, size_t actual) {
  if (exp != actual)
    throw std::invalid_argument("Wrong number of arguments for command " + cmd +
                                ". Expected: " + std::to_string(exp) +
                                ". Got: " + std::to_string(actual));
}

int main(int argc, char *argv[]) {
  std::cout << std::fixed << std::setprecision(2);

  imrepo::ImageIndex<cv::img_hash::PHash> image_index;
  std::string input;

  image_index.Debug("./test-data/102900.jpg");
  image_index.Debug("./test-data/101100.jpg");

  std::cout << "Current path: " << std::filesystem::current_path() << std::endl;

  while (true) {
    std::cout << ">>> " << std::flush;
    getline(std::cin, input);

    if (cin.eof())
      break;

    auto tokens = split(input);
    if (tokens.empty())
      continue;

    try {
      auto start = std::chrono::high_resolution_clock::now();
      if (tokens[0] == "exit") {
        break;
      } else if (tokens[0] == "load") {
        check_arg_count("load", 2, tokens.size());
        image_index.Load(tokens[1]);
        std::cout << "Loaded " << image_index.size() << " images." << std::endl;
      } else if (tokens[0] == "search") {
        check_arg_count("search", 3, tokens.size());
        auto search_result =
            image_index.Search(tokens[1], std::stoi(tokens[2]));
        for (const auto &entry : search_result)
          std::cout << entry.first << " -- distance: " << entry.second << '\n';
        std::cout << std::flush;
      } else if (tokens[0] == "display") {
        image_index.Display();
      } else {
        throw std::invalid_argument("Invalid command.");
      }
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration =
          std::chrono::duration_cast<std::chrono::microseconds>(stop - start)
              .count();
      std::cout << "\033[1;32m"
                << "Took: " << duration << "us\033[0m" << std::endl;
    } catch (const std::exception &e) {
      std::cout << "Error: " << e.what() << std::endl;
    }
  }
  return 0;
}
