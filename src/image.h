#ifndef IMAGE_H_
#define IMAGE_H_

#include <bitset>
#include <string>
#include <utility>

#include <opencv4/opencv2/core.hpp>

namespace imrepo {

struct Image {
  using Hash = std::bitset<64>;

  std::string name;
  Hash hash;

  Image(const std::string &imname, const cv::Mat &imhash)
      : name(std::move(imname)), hash(toHash(imhash)) {}
  Image(const std::string &imname, const Hash &imhash)
      : name(std::move(imname)), hash(std::move(imhash)) {}

  static Hash toHash(const cv::Mat &imhash) {
    uint64_t hash = *imhash.ptr<uint64_t>(0);
    return Hash(hash);
  }
};

} // namespace imrepo

#endif
