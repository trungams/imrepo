#ifndef INDEX_H_
#define INDEX_H_

#include <bitset>
#include <filesystem>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/img_hash.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>

#include "src/image.h"
#include "src/vptree.h"

namespace imrepo {

template <class HashFunc> class ImageIndex {
private:
  struct HammingDistance {
    int operator()(const Image &a, const Image &b) {
      Image::Hash x = a.hash ^ b.hash;
      return static_cast<int>(x.count());
    }
  };

public:
  ImageIndex() : hash_func_(HashFunc::create()) {}

  void Load(const std::string &dir) {
    std::filesystem::path path(dir);
    if (!std::filesystem::exists(path))
      throw std::runtime_error("Directory does not exist!");

    cv::Mat img, imhash;
    for (const auto &file : std::filesystem::directory_iterator(path)) {
      img = cv::imread(file.path().string());
      imhash = Hash(img);
      images_.emplace_back(file.path().string(), std::move(imhash));
    }

    vp_tree_.Create(images_);
  }

  std::vector<std::pair<std::string, int>> Search(const std::string &filename,
                                                  int k = 1) {
    cv::Mat img = cv::imread(filename);
    cv::Mat imhash = Hash(img);
    std::vector<std::pair<std::string, int>> result;

    Image target = Image(filename, imhash);
    HammingDistance f;
    std::sort(images_.begin(), images_.end(),
              [&](const auto &a, const auto &b) {
                return f(target, a) < f(target, b);
              });
    for (int i = 0; i < k; i++)
      result.emplace_back(images_[i].name, f(images_[i], target));

    return result;
  }

  void Display() {}

  size_t size() const { return images_.size(); }

  void Debug(const std::string &filename) {
    cv::Mat img = cv::imread(filename);
    cv::Mat imhash = Hash(img);
    std::cout << imhash.rows << ' ' << imhash.cols << ' '
              << imhash.isContinuous() << '\n';
    uint64_t *arr = imhash.ptr<uint64_t>(0);
    std::cout << std::hex << *arr << '\n';
    Image::Hash h = Image::toHash(imhash);
    std::cout << std::hex << h.to_ullong() << std::endl;
    std::cout << std::dec;
  }

private:
  VPTree<Image, int, HammingDistance> vp_tree_;
  std::vector<Image> images_;
  cv::Ptr<cv::img_hash::ImgHashBase> hash_func_;

  cv::Mat Hash(const cv::Mat &input) {
    cv::Mat output;
    hash_func_->compute(input, output);
    return std::move(output);
  }
};

} // namespace imrepo

#endif
