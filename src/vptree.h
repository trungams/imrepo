#ifndef VP_TREE_H_
#define VP_TREE_H_

#include <algorithm>
#include <chrono>
#include <iterator>
#include <limits>
#include <memory>
#include <queue>
#include <random>
#include <stdexcept>
#include <utility>
#include <vector>

namespace imrepo {

template <class T, class Dist, typename DistCalculator> class VPTree {
private:
  struct Node {
    int index;
    Dist threshold;
    Node *left;
    Node *right;

    Node() : index(0), threshold(), left(nullptr), right(nullptr) {}
    ~Node() {
      delete left;
      delete right;
    }
  };

  struct HeapNode {
    int index;
    Dist dist;

    explicit HeapNode(int index, Dist d) : index(index), dist(d) {}
    bool operator<(const HeapNode &rhs) const { return dist < rhs.dist; }
  };

public:
  VPTree()
      : root_(nullptr),
        rng(std::chrono::steady_clock::now().time_since_epoch().count()) {}
  ~VPTree() { delete root_; };

  void Create(const std::vector<T> &items) {
    delete root_;
    items_ = std::move(items);
    root_ = BuildFromPoints(0, items_.size());
  }

  std::vector<std::pair<T, Dist>> Search(const T &target, int k = 1) {
    if (root_ == nullptr)
      throw std::logic_error("VP Tree has not been initialized.");

    std::priority_queue<HeapNode> pq;
    tau_ = MAX_TAU;
    std::vector<std::pair<T, Dist>> result;

    Search(root_, target, k, pq);
    while (!pq.empty()) {
      result.emplace_back(items_[pq.top().index], pq.top().dist);
      pq.pop();
    }

    reverse(result.begin(), result.end());
    return result;
  }

  void Debug() {
    std::cerr << "Size: " << items_.size() << '\n';
    for (auto item : items_)
      std::cerr << item.to_string() << '\n';
    DFS(root_);
  }

  void DFS(Node *node) {
    if (node == nullptr)
      return;
    std::cerr << "Node: index: " << node->index
              << ", threshold: " << node->threshold
              << ", left: " << (node->left ? node->left->index : -1)
              << ", right: " << (node->right ? node->right->index : -1) << '\n';
    DFS(node->left);
    DFS(node->right);
  }

private:
  static constexpr Dist MAX_TAU = std::numeric_limits<Dist>::max();

  std::vector<T> items_;
  Node *root_;
  Dist tau_;
  DistCalculator distance_;
  std::mt19937 rng;

  Node *BuildFromPoints(int lower, int upper) {
    if (lower == upper)
      return nullptr;

    auto node = new Node();
    node->index = lower;

    if (upper - lower > 1) {
      int i = std::uniform_int_distribution<int>(lower + 1, upper - 1)(rng);
      std::swap(items_[lower], items_[i]);

      int median = (upper + lower) / 2;
      std::nth_element(
          std::next(items_.begin(), lower + 1),
          std::next(items_.begin(), median), std::next(items_.begin(), upper),
          [this, lower](const T &a, const T &b) {
            return distance_(items_[lower], a) < distance_(items_[lower], b);
          });

      node->threshold = distance_(items_[lower], items_[median]);
      node->index = lower;
      node->left = BuildFromPoints(lower + 1, median);
      node->right = BuildFromPoints(median, upper);
    }

    return node;
  }

  void Search(Node *node, const T &target, int k,
              std::priority_queue<HeapNode> &pq) {
    if (node == nullptr)
      return;

    Dist dist = distance_(items_[node->index], target);

    if (dist < tau_) {
      if (static_cast<int>(pq.size()) == k)
        pq.pop();
      pq.push(HeapNode(node->index, dist));
      if (static_cast<int>(pq.size()) == k)
        tau_ = pq.top().dist;
    }

    if (node->left == nullptr && node->right == nullptr)
      return;

    if (dist < node->threshold) {
      if (dist - tau_ <= node->threshold)
        Search(node->left, target, k, pq);
      if (tau_ == MAX_TAU || dist + tau_ >= node->threshold)
        Search(node->right, target, k, pq);
    } else {
      if (tau_ == MAX_TAU || dist + tau_ >= node->threshold)
        Search(node->right, target, k, pq);
      if (dist - tau_ <= node->threshold)
        Search(node->left, target, k, pq);
    }
  }
};

} // namespace imrepo

#endif
