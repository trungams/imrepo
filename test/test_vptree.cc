#include <algorithm>
#include <bitset>
#include <chrono>
#include <iostream>
#include <limits>
#include <random>
#include <vector>

#include <gtest/gtest.h>

#include "src/vptree.h"

TEST(VPTreeTest, knnSearch) {
  using Hash = std::bitset<64>;
  struct Calc {
    int operator()(const Hash &a, const Hash &b) {
      Hash x = a ^ b;
      return static_cast<int>(x.count());
    }
  };

  int N = 1000, k = 500;
  std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<uint64_t> dist(
      0, std::numeric_limits<uint64_t>::max());

  std::vector<Hash> hash_values;
  for (int i = 0; i < N; i++)
    hash_values.emplace_back(dist(rng));

  imrepo::VPTree<Hash, int, Calc> tree;
  tree.Create(hash_values);

  Hash target(dist(rng));
  Calc distance;

  std::sort(hash_values.begin(), hash_values.end(),
            [&](const Hash &a, const Hash &b) {
              return distance(target, a) < distance(target, b);
            });

  std::vector<Hash> exp_knn(hash_values.begin(), hash_values.begin() + k);

  // quick benchmark
  // auto t1 = std::chrono::high_resolution_clock::now();
  auto knn = tree.Search(target, k);
  // auto t2 = std::chrono::high_resolution_clock::now();
  // auto duration =
  //     std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

  // std::cerr << duration << std::endl;

  ASSERT_EQ(exp_knn.size(), knn.size())
      << "VPTree could not find exactly k neighbors";
  for (size_t i = 0; i < exp_knn.size(); i++) {
    EXPECT_EQ(distance(exp_knn[i], target), knn[i].second);
  }
}